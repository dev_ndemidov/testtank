﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Unit : MonoBehaviour
{
    public List<Ability> abilities = new List<Ability>();

    private void Awake()
    {
        abilities = GetComponentsInChildren<Ability>(true).ToList();
        abilities.ForEach(x => x.Enable(this));
    }

    public T GetAbility<T>() where T : Ability
    {
        return abilities.Find(search => search is T) as T;
    }
}
