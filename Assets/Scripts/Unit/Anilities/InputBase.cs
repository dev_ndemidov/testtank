﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputBase : Ability
{
    public delegate void InputUse();
    public event InputUse OnUse;
    public delegate void InputCancel();
    public event InputCancel OnCancel;

    public delegate void InputDirection(Vector2 direction);
    public event InputDirection OnDirection;

    public delegate void InputSpell();
    public event InputSpell OnSpell;

    public virtual void Use()
    {
        OnUse?.Invoke();
    }

    public virtual void Cancel()
    {
        OnCancel?.Invoke();
    }

    public virtual void Spell()
    {
        OnSpell?.Invoke();
    }

    public virtual void SetDirection(Vector2 direction)
    {
        OnDirection?.Invoke(direction);
    }
}
