﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : Ability
{
    public new Camera camera;
    public Transform target;
    public float distance = 5f;
    public float height = 2f;
    public float smoothSpeed = 5f;
    public Vector3 offset;

    Vector3 distanceClamp = new Vector3(.5f, 5f);
    Vector3 yLimit = new Vector3(-50, 80);

    float x;

    public override void Activate()
    {
        base.Activate();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        SetTarget(unit.transform, 2f, 1.5f);
    }

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
    }

    public void SetTarget(Transform newTarget, float distance, float height)
    {
        target = newTarget;
        this.distance = distance;
        this.height = height;
    }
    
    void LateUpdate()
    {
        x += Input.GetAxis("Mouse X") * smoothSpeed * distance * 0.02f;
        Quaternion rotation = Quaternion.Euler(0, x, 0);
        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * negDistance + target.position + offset;
        camera.transform.rotation = rotation;
        camera.transform.position = position;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}
