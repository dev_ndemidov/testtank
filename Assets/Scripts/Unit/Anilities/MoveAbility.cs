﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAbility : Ability
{
    public InputBase inputBase;
    public new Rigidbody rigidbody;

    public float moveSpeed;
    public float rotationSpeed;

    public override void Activate()
    {
        base.Activate();
        inputBase.OnDirection += Move;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        inputBase.OnDirection -= Move;
    }

    private void Move(Vector2 direction)
    {
        rigidbody.velocity = unit.transform.forward * direction.y * Time.deltaTime * moveSpeed;
        rigidbody.angularVelocity = unit.transform.up * direction.x * Time.deltaTime * rotationSpeed;
    }
}
