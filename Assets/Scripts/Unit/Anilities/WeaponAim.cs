﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WeaponAim : Ability
{
    public Vector2 xLimit = new Vector2(-80, 0);
    public Vector2 yLimit = new Vector2(-20, 20);

    public InputBase inputBase;
    public Transform weapon;
    public float speedRotation;

    private Vector3 currentPosition;
    private Vector3 deltaPosition;
    private Vector3 lastPosition;
    
    public float bulletSpeed = 20f;
    
    public float h = 1;
    
    public LineRenderer lineRenderer;
    public GameObject bulletObj;

    public bool hitInLocation;
    public Image aimIcon;

    public override void Activate()
    {
        base.Activate();
        inputBase.OnSpell += Shoot;
    }

    public override void Deactivate()
    {
        base.Deactivate();
        inputBase.OnSpell -= Shoot;
    }
    
    private void Update()
    {
        Ray rayMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
        float screenMidPoint = (weapon.position - Camera.main.transform.position).magnitude * 0.5f;
        Vector3 lookForward = rayMouse.origin + rayMouse.direction * screenMidPoint + Camera.main.transform.forward * Camera.main.farClipPlane;
        var lookPos = lookForward - weapon.position;
        var rotation = Quaternion.LookRotation(lookPos);
        float vertical = rotation.eulerAngles.y;

        float horizontal = (weapon.localEulerAngles + Vector3.right * -Input.GetAxis("Mouse Y") * Time.deltaTime * speedRotation).x;

        weapon.rotation = Quaternion.Euler(new Vector3(horizontal, vertical, 0));

        float x = ClampAngle(weapon.localEulerAngles.x, xLimit.x, xLimit.y);
        float y = ClampAngle(weapon.localEulerAngles.y, yLimit.x, yLimit.y);

        if (y >= yLimit.y && y < (360 + yLimit.x) || (y <= (360 + yLimit.x) && y > yLimit.y))
        {
            float sign = y == yLimit.y ? 1 : -1;
            unit.GetAbility<InputBase>()?.SetDirection(Vector2.up * Input.GetAxis("Vertical") + Vector2.right * sign);
        }
        weapon.localEulerAngles = new Vector3(x, y, 0);

        DrawShootPath();
        aimIcon.color = hitInLocation ? Color.green : Color.red;
        lineRenderer.startColor = hitInLocation ? Color.green : Color.red;
        var hits = Physics.SphereCastAll(lineRenderer.GetPosition(lineRenderer.positionCount - 1) + Vector3.up * 10, 0.12f,Vector3.down, Mathf.Infinity);
        if (hits.Length > 0)
        {
            if (hits.ToList().Where(search => search.collider.tag == "Bullet").Count() == hits.Length)
            {
                hitInLocation = false;
            }
            else
            {
                hitInLocation = true;
            }
        }
        else
        {
            hitInLocation = false;
        }
    }

    void DrawShootPath()
    {
        float timeToHitTarget = CalculateTimeToHit();
        
        int maxIndex = Mathf.RoundToInt(timeToHitTarget / h);

        lineRenderer.positionCount = maxIndex;
        
        Vector3 currentVelocity = weapon.forward * bulletSpeed;
        Vector3 currentPosition = weapon.position;

        Vector3 newPosition = Vector3.zero;
        Vector3 newVelocity = Vector3.zero;

        for (int index = 0; index < maxIndex; index++)
        {
            lineRenderer.SetPosition(index, currentPosition);
            BackwardEuler(h, currentPosition, currentVelocity, out newPosition, out newVelocity);
            currentPosition = newPosition;
            currentVelocity = newVelocity;
        }
    }

    public float CalculateTimeToHit()
    {
        Vector3 currentVelocity = weapon.forward * bulletSpeed;
        Vector3 currentPosition = weapon.position;

        Vector3 newPosition = Vector3.zero;
        Vector3 newVelocity = Vector3.zero;
        
        float time = 0f;
        for (time = 0f; time < 30f; time += h)
        {
            BackwardEuler(h, currentPosition, currentVelocity, out newPosition, out newVelocity);
            
            if (newPosition.y < currentPosition.y && newPosition.y < unit.transform.position.y)
            {
                time += h * 2f;
                break;
            }

            currentPosition = newPosition;
            currentVelocity = newVelocity;
        }

        return time;
    }

    public void BackwardEuler(float h, Vector3 currentPosition, Vector3 currentVelocity,out Vector3 newPosition,out Vector3 newVelocity)
    {
        Vector3 acceleartionFactor = Physics.gravity;
        newVelocity = currentVelocity + h * acceleartionFactor;
        newPosition = currentPosition + h * newVelocity;
    }

    void Shoot()
    {
        if (hitInLocation)
        {
            GameObject newBullet = Instantiate(bulletObj, weapon.position, weapon.rotation) as GameObject;
            newBullet.GetComponent<Rigidbody>().velocity = bulletSpeed * weapon.forward;
            Destroy(newBullet, 10f);
        }
    }

    public float ClampAngle(float angle, float min, float max)
    {
        angle = Mathf.Repeat(angle, 360);
        min = Mathf.Repeat(min, 360);
        max = Mathf.Repeat(max, 360);
        bool inverse = false;
        var tmin = min;
        var tangle = angle;
        if (min > 180)
        {
            inverse = !inverse;
            tmin -= 180;
        }
        if (angle > 180)
        {
            inverse = !inverse;
            tangle -= 180;
        }
        var result = !inverse ? tangle > tmin : tangle < tmin;
        if (!result)
            angle = min;

        inverse = false;
        tangle = angle;
        var tmax = max;
        if (angle > 180)
        {
            inverse = !inverse;
            tangle -= 180;
        }
        if (max > 180)
        {
            inverse = !inverse;
            tmax -= 180;
        }

        result = !inverse ? tangle < tmax : tangle > tmax;
        if (!result)
            angle = max;
        return angle;
    }
}
