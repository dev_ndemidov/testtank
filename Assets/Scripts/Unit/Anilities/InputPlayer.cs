﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputPlayer : InputBase
{
    void Update()
    {
        var x = Input.GetAxis("Horizontal");
        var y = Input.GetAxis("Vertical");
        var direction = new Vector2(x, y);
        SetDirection(direction);

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Spell();
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {
            Use();
        }

        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            Cancel();
        }
    }
}
