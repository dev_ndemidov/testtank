﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Ability : MonoBehaviour
{
    [HideInInspector]
    public Unit unit;

    public System.Action onActivate;
    public System.Action onDeactivate;

    public System.Action onEnter;
    public System.Action onExit;

    public UnityEvent onEnable;
    public UnityEvent onDisable;

    public virtual void Enable(Unit u)
    {
        unit = u;
        this.gameObject.SetActive(true);
    }

    public virtual void Disable()
    {
        this.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        onEnable.Invoke();
    }

    private void OnDisable()
    {
        onDisable.Invoke();
    }

    public virtual void Activate()
    {
        onActivate?.Invoke();
    }

    public virtual void Deactivate()
    {
        onDeactivate?.Invoke();
    }

    public virtual void OnEnter()
    {
        onEnter?.Invoke();
    }

    public virtual void OnExit()
    {
        onExit?.Invoke();
    }
}