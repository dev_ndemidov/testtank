﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject explosionEffect;

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
        GameObject effect = Instantiate(explosionEffect, transform.position, Quaternion.identity) as GameObject;
        Destroy(effect, .6f);
    }
}
